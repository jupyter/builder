import sys
import time
from podman.errors import DockerException
from urllib.parse import urlparse
import random
import string
import yaml

class BlockString(str):
    pass

def change_style(style, representer):
    def new_representer(dumper, data):
        scalar = representer(dumper, data)
        scalar.style = style
        return scalar
    return new_representer


represent_literal_str = change_style('|', yaml.representer.SafeRepresenter.represent_str)
yaml.add_representer(BlockString, represent_literal_str)

def random_string(l):
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(l)])

class DockerError(RuntimeError):

    def __init__(self, line):
        super().__init__(line['errorDetail']['message'])

        self.line = line

class DockerLogger:

    def __init__(self, prefix):
        self.prefix = prefix
        self.status_lines = dict()

    def log(self, line):
        if 'stream' in line:
            out = line['stream']
        elif 'errorDetail' in line:
            raise DockerException(line)
        elif 'status' in line:
            sts = line['status']

            if 'id' in line:
                id = line['id']

                if id in self.status_lines:
                    off = len(self.status_lines) - self.status_lines[id]

                    progress = line.get('progress', '')

                    out = f'\033[{off}A\r\033[K{id}: {sts} {progress}\033[{off}B\r'
                else:
                    self.status_lines[id]  = len(self.status_lines)

                    out = f'{id}: {sts}\n'
            else:
                out = sts + '\n'
        elif 'aux' in line and False: # redundant output..
            out = ', '.join([ f'{k}: {v}' for k, v in line['aux'].items() ]) + '\n'
        else:
            return

        sys.stdout.write(out)

def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc])
    except:
        return False


class LogSection:

    def __init__(self, desc):
        self.desc = desc
        self.section_id = random_string(6)

    def __enter__(self):
        print("section_start:{}:{}\r\x1b[0K{}".format(int(time.time()), self.section_id, self.desc))

    def __exit__(self, type, value, traceback):
        print("section_end:{}:{}\r\x1b[0K".format(int(time.time()), self.section_id))
