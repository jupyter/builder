#!/bin/env python3

import argparse
import yaml
import json
import sys
import os
import logging
import podman
import time
import git
import re
import datetime
import subprocess
from subprocess import DEVNULL,STDOUT

import jupyterhub_profile.config as c
from jupyterhub_profile.profile_list import ProfileList
from jupyterhub_profile.profile import Profile

def parse_args():
    parser = argparse.ArgumentParser(prog='jupyterhub-profile')

    parser.add_argument('--dir', '-d', metavar='DIR', default='profiles/', help='Directory with profile files')
    parser.add_argument('--profiles', '-p', default='all', metavar='PROFILES', help='Select profiles. All if omitted')
    parser.add_argument('--verbose', '-v', action='count', default=1)
    parser.add_argument('--no-dependencies', '-n', action='store_true', default=False, help='Should we build dependencies before?')
    parser.add_argument('--docker-timeout', '-t', metavar='TIMEOUT', type=int, default=45*60, help='Default timeout for Docker API calls, in seconds.')
 
    subparsers = parser.add_subparsers(help='sub-command help', metavar='CMD', dest='cmd')

    parser_graph = subparsers.add_parser('graph', help='Visuzalize profile dependencies via Graphviz')
    parser_graph.add_argument('--format', '-f', type=str, default='svg', choices=['canon', 'cmap', 'cmapx', 'cmapx_np', 'dia', 'dot', 'fig', 'gd', 'gd2', 'gif', 'hpgl', 'imap', 'imap_np', 'ismap', 'jpe', 'jpeg', 'jpg', 'mif', 'mp', 'pcl', 'pdf', 'pic', 'plain', 'plain-ext', 'png', 'ps', 'ps2', 'svg', 'svgz', 'vml', 'vmlz', 'vrml', 'vtx', 'wbmp', 'xdot', 'xlib'])
    parser_graph.add_argument('--layout', '-l', type=str, default='dot', choices=['neato', 'dot', 'twopi', 'circo', 'fdp', 'nop'])
    parser_graph.add_argument('file', metavar='FILE', type=str, default='graph.svg')

    parser_build = subparsers.add_parser('build', help='Build Docker images for selected profiles')
    parser_build.add_argument('--skip-clone', '-s', action='store_true', help='Skip clone of Git repo if it already exists.')

    parser_get = subparsers.add_parser('get', help='Generate Jupyter Profile configuration')
    parser_get.add_argument('data', metavar='{profile_list,definition}', type=str, default='profile_list', choices=['profile_list', 'definition'])
    parser_get.add_argument('--format', '-f', type=str, default='yaml', choices=['yaml', 'json'])

    parser_pipeline = subparsers.add_parser('pipeline', help='Generate CI job configuration')
    parser_pipeline.add_argument('--format', '-f', type=str, default='gitlab', choices=['gitlab'])
    parser_pipeline.add_argument('--include-disabled', '-i', help='Include disabled profiles as manually triggered jobs')

    parser_lint = subparsers.add_parser('lint', help='Validate profile definitions')

    parser_query = subparsers.add_parser('query', help='Query the profile descriptions')
    parser_query.add_argument('--query', '-q', metavar='QUERY', help='A JMESpath query')
    parser_query.add_argument('--format', '-f', help='Output format', choices=[ 'json', 'yaml', 'raw' ], default='json')

    parser_versions = subparsers.add_parser('versions', help='Extract Python packages and version from Docker image')
    parser_versions.add_argument('--repo', '-r', required=True, choices=['conda', 'pip', 'jupyter-labextensions', 'jupyter-serverextensions', 'jupyter-kernelspecs'])
    
    args = parser.parse_args()

    if not args.cmd:
        parser.print_help()
        print('\nPlease provide a sub-command')
        sys.exit(-1)

    args.profiles = args.profiles.split(',')

    return args

def check_version():
    if not (sys.version_info.major == 3 and sys.version_info.minor >= 5):
        logging.error("This script requires Python 3.5 or higher!")
        logging.error("You are using Python {}.{}.".format(sys.version_info.major, sys.version_info.minor))
        sys.exit(1)


def _converter(o):
            if isinstance(o, datetime.datetime):
                return o.__str__()

            if isinstance(o, Profile):
                return o.slug


def main():
    args = parse_args()

    args.verbose = 30 - (10*args.verbose) if args.verbose > 0 else 0

    logging.basicConfig(level=args.verbose, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    check_version()

    for acc in c.DOCKER_ACCOUNTS:
        subprocess.Popen(["podman", "login", acc['registry'], '--username', acc['username'], '--password', acc['password']], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

    pl = ProfileList(
        args.dir, args.profiles,
        dependencies=(not args.no_dependencies),
        lint=(args.cmd == 'lint')
    )

    if args.cmd == 'build':
        try:
            os.mkdir('repos')
        except FileExistsError:
            pass

        pl.build_images(args.skip_clone)

    elif args.cmd == 'graph':
        pl.graph(args.file, args.format, args.layout)

    elif args.cmd == 'get':
        if args.data == 'profile_list':
            o = [ p.config for p in pl if p.enabled ]
        elif args.data == 'definition':
            o = [ p.data for p in pl if p.enabled ]
        
        if args.format == 'yaml':
            yaml.dump(o, sys.stdout)
        elif args.format == 'json':
            json.dump(o, sys.stdout, default=_converter, **c.JSON_FORMAT)

    elif args.cmd == 'pipeline':
        pl = pl.get_pipeline(args.format, args.include_disabled)

        yaml.dump(pl, sys.stdout)

    elif args.cmd == 'query':
        results = pl.query(args.query)

        if args.format == 'json':
            json.dump(results, sys.stdout, default=_converter, **c.JSON_FORMAT)
        elif args.format == 'yaml':
            sys.stdout.write(yaml.dump(results))
        elif args.format == 'raw':
            sys.stdout.write('\n'.join(results) + '\n')

    elif args.cmd == 'versions':
        o = [ p for p in pl if p.slug == args.profiles[0] ]

        if len(o) != 1:
            raise RuntimeError('Please specify exactly 1 profile via --profile/-p argument')

        vers = o[0].get_versions(args.repo, client)

        json.dump(vers, sys.stdout, **c.JSON_FORMAT)


if __name__ == '__main__':
    main()
