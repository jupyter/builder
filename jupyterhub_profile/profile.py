import git
import sys
import yaml
import json
import os
import shutil
import collections
import re
import datetime
import logging
import base64
import tempfile
import podman
import subprocess

from pkg_resources import resource_filename, resource_string
from podman.errors import DockerException, ContainerError
from termcolor import colored
from furl import furl

import jupyterhub_profile.config
from jupyterhub_profile.config import *
from jupyterhub_profile.util import *
from jupyterhub_profile.profile_validator import ProfileValidator
from jupyterhub_profile.dag import Node

class Profile(Node):

    def __init__(self, y, en=True, fn=None):

        if not Profile.lint(y):
            sys.exit(-1)

        z = Profile.validator.normalized(y)

        self.slug = z.get('slug')
        self.display_name = z.get('display_name')
        self.description = z.get('description')
        self.course = z.get('course')
        self.kubespawner_override = z.get('kubespawner', {})
        self.notebooks = z.get('notebooks', {})
        self.image = z.get('image', {})
        self.hidden = z.get('hidden', False)
        self.acl = z.get('acl', [])
        self.enabled = en
        self.fn = fn

        super().__init__(self.slug)

        self._data = z
        
        self.dir = 'repos/' + self.slug

        if 'repo' in self.image:
            self.image['repository'] = DOCKER_REGISTRY + '/' + self.slug
        elif 'base' in self.image:
            self.image['repository'] = DOCKER_REGISTRY + '/' + self.image['base']
        
        self.image['id'] = None

        if fn:
            if 'CI' in os.environ:
                sha1   = os.environ['CI_COMMIT_SHORT_SHA']
                branch = os.environ['CI_COMMIT_REF_NAME']
            else:
                repo = git.Repo(search_parent_directories=True)
                sha1 = repo.head.object.hexsha[0:8]
                branch = repo.active_branch.name

            if 'tags' in self.image:
                self.image['tags'].append('git-' + sha1)
                self.image['tags'].append(branch)

                self.image['tags'].append('latest')

    def __lt__(self, c):
        if self.priority < c.priority:
            return True

        if self.fn < c.fn:
            return True
        
        return self.slug < c.slug

    @property
    def data(self):
        return {
             **self._data,
            'image': self.image,
            'kubespawner': self.kubespawner,
            'enabled': self.enabled,
            'dir': self.dir,
            'fn': self.fn
        }

    @classmethod
    def _get_repo_url(cls, repo):
        # Add username and password to URL
        if 'username' in repo and 'password' in repo:
            u = furl(repo['url'])

            u.username = repo['username']
            u.password = repo['password']

            return str(u)
        else:
            return repo['url']

    @classmethod
    def lint(cls, p):
        if cls.validator.validate(p):
            return True
        else:
            logging.error('Failed to validate profile: %s', p['slug'])
            y = yaml.dump(cls.validator.errors, indent=2)
            sys.stdout.write(''.join([ '  ' + l for l in y.splitlines(keepends=True)]))

            return False

    def __repr__(self):
        return 'Profile(slug=%s)' % (self.slug)

    def clone_repo(self):
        """Clone Git Repo"""

        repo = self.image['repo']
        url = self._get_repo_url(repo)

        # Clone Repo
        if os.path.exists(self.dir):
            shutil.rmtree(self.dir)

        # Write ssh identity to temporary file
        ssh_id_file = None
        git_env={}
        if 'ssh_identity' in self.image['repo']:
            ssh_id_file = tempfile.NamedTemporaryFile()

            ssh_id = self.image['repo']['ssh_identity'] + '\n'
            ssh_id = ssh_id.encode('ascii')

            ssh_id_file.write(ssh_id)
            ssh_id_file.flush()

            git_env['GIT_SSH_COMMAND'] = f'ssh -i {ssh_id_file.name} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'

        r = git.Repo.clone_from(
            url, self.dir, env=git_env,
            recurse_submodules=repo['recursive']
        )

        if ssh_id_file is not None:
            ssh_id_file.close()  # deletes temp file

        # Detach HEAD if we choose to 
        if 'ref' in repo:
            if re.match('^[A-Fa-f0-9]{7,}$', repo['ref']):
                ref = repo['ref']
            else:
                ref = 'origin/' + repo['ref']

            r.head.reference = r.commit(ref)
            r.head.reset(index=True, working_tree=True)
        else:
            repo['ref'] = r.head.reference

    def build_image(self):
        """Build the Docker image"""

        dockerfile = self.image.get('dockerfile', 'Dockerfile')
        buildargs = self.image.get('buildargs', {})
        repository = self.image['repository']

        if 'base' in self.image:
            base = self.image['base']

            buildargs['BASE_IMAGE'] = base.image['repository']
            buildargs['BASE_CONTAINER'] = buildargs['BASE_IMAGE'] # Compat for jupyter/docker-stacks images

            logging.info('Using base image: %s', buildargs['BASE_IMAGE'])

        path = self.dir
        ctx =  self.image.get('context')
        if ctx:
            path += '/' + ctx

        try:
            l = DockerLogger('docker.build')

            command = ['podman', 'build']
            for buildarg in buildargs.items():
                command.append(f'--build-arg={buildarg[0]}={buildarg[1]}')
            command.append('--rm')
            command.append('--pull-always')
            command.append('--events-backend=file')
            command.append('--cgroup-manager')
            command.append('cgroupfs')
            command.append('--format')
            command.append('docker')
            command.append('--file')
            command.append(os.path.join(path, dockerfile))
            command.append('--tag')
            command.append(f'{repository}')
            p = subprocess.Popen(' '.join(command), shell=True)
            p.communicate()

        except DockerException as exc:
            logging.error('Failed to build %s: %s', repository, exc)
            sys.exit(-1)

    def push_image(self):
        repository = self.image['repository']
        tags = self.image['tags']

        for tag in tags:
            logging.info('Pushing tag: %s', tag)
            command = ['podman', 'tag']
            command.append(repository)
            command.append(f'{repository}:{tag}')
            p = subprocess.Popen(' '.join(command), shell=True)
            p.communicate()

            try:
                l = DockerLogger('docker.push')

                command = ['podman', 'push']
                command.append(f'{repository}')
                p = subprocess.Popen(' '.join(command), shell=True)
                p.communicate()


            except DockerException as exc:
                logging.error('Failed to push %s: %s', f'{repository}:{tag}', exc)
                sys.exit(-1)

    def build(self, skip_clone):
        if 'repo' not in self.image or not self.enabled:
            logging.info("Nothing to build for: %s", self.slug)
            return
        else:
            logging.info('Building: %s', self.slug)

        if not skip_clone:
            with LogSection('Clone Git repo: ' + self.image['repo']['url']):
                self.clone_repo()

        with LogSection('Build Image: ' + self.image['repository']):
            self.build_image()
        
        with LogSection('Push Image: ' + self.image['repository']):
            self.push_image()

    def get_versions(self, repo, docker):
        # Elegant but broken in our Kubernetes CI runners..
        def _helper_script_volumes(script):
            return {
                'command': [ 'python3', '/' + script ],
                'volumes': {
                    resource_filename(__name__, os.path.join('helpers', script)) : {
                        'bind': '/' + script,
                        'mode': 'ro'
                    }
                }
            }

        def _helper_script(script):
            scr = resource_string(__name__, os.path.join('helpers', script))
            src_b64 = base64.b64encode(scr).decode()
            return {
                'command': [ 'sh', '-c', f'echo {src_b64} | base64 -d | python3 -' ]
            }

        opts = {
            'conda': {
                'command': [ 'conda', 'list', '--json' ]
            },
            'pip': {
                'command': [ 'pip', 'list', '--format',  'json' ]
            },
            'jupyter-kernelspecs': {
                'command' : [ 'jupyter', 'kernelspec', 'list', '--json' ]
            },
            # We need some helper scripts here to return the list in JSON format
            'jupyter-serverextensions': _helper_script('jupyter_serverextensions.py'),
            'jupyter-labextensions':    _helper_script('jupyter_labextensions.py')
        }

        try:
            out = docker.containers.run(
                image=self.image['repository'],
                remove=True,
                stderr=True,
                detach=False,
                **opts[repo]
            )

            versions = json.loads(out)
        except ContainerError as e:
            logging.error('Failed to get %s versions:', repo)
            logging.error("Command '{}' in image '{}' returned non-zero exit "
               "status {}".format(e.command, e.image, e.exit_status))
            sys.stderr.write(e.stderr.decode())
            sys.exit(-1)

        except json.decoder.JSONDecodeError as e:
            logging.error('Failed to parse JSON: %s', e)
            sys.stderr.write(out.decode())
            sys.exit(-1)

        if repo == 'jupyter-kernelspecs':
            versions = [ { 'name': name, **spec } for name, spec in versions['kernelspecs'].items() ]

        return versions

    @property
    def gitlab_ci_job(self):

        # Is there something to build for this profile?
        if 'repo' not in self.image:
            return

        repos = [ 'conda', 'pip', 'jupyter-labextensions', 'jupyter-serverextensions', 'jupyter-kernelspecs' ]

        j = {
            'image': DOCKER_BUILDER_IMAGE,
            'before_script': [
                'mkdir -p versions'
            ],      
            'script': [
                f'jupyterhub-profile -n -p {self.slug} build'
            ] + [
                f'jupyterhub-profile -n -p {self.slug} versions -r {repo} > versions/{repo}.json || true' for repo in repos
            ],
            'artifacts': {
                'paths': [
                    'versions/*.json'
                ]
            }
        }

        if 'base' in self.image:
            j['needs'] = [ self.image['base'].slug ]

        if not self.enabled:
            j['when'] = 'manual'

        return j

    @property
    def config(self):
        cfg = {
            'slug': self.slug,
            'display_name': self.display_name,
            'hidden': self.hidden,
            'acl': self.acl,
            'kubespawner_override': self.kubespawner,
        }

        desc = []

        if self.description:
            desc.append(self.description)

        if 'faculty' in self.course:
            desc.append(self.course['faculty']['name'])

        if 'studies' in self.course:
            desc.append(self.course['studies'])

        if len(desc) > 0:
            cfg['description'] = ' - '.join(desc)

        return cfg

    @property
    def kubespawner(self):
        e = {
            'JUPYTERHUB_PROFILE': self.slug
        }

        y = {
            'extra_labels': {
                'profile': self.slug
            },
            'environment': e
        }
        
        if 'repository' in self.image:
            y['image'] = self.image['repository'] #+ ':' + self.image['tags'][0]

        if self.notebooks != {}:
            e['JUPYTERHUB_REPO_URL'] = self._get_repo_url(self.notebooks['repo'])
            e['JUPYTERHUB_REPO_REF'] = self.notebooks['repo']['ref']

            if self.notebooks['pull']:
                e['JUPYTERHUB_REPO_PULL'] = 'true'

            if 'ssh_identity' in self.notebooks['repo']:
                ssh_id = self.notebooks['repo']['ssh_identity'].encode('ascii')
                ssh_id_b64 = base64.b64encode(ssh_id).decode('ascii')
                e['JUPYTERHUB_REPO_SSH_ID'] = ssh_id_b64

            if 'index' in self.notebooks:
                y['default_url'] = '/lab/tree/' + self.slug + '/' + self.notebooks['index']

        if 'pull' in self.image:
            y['image_pull_policy'] = self.image['pull']

        return {**y, **self.kubespawner_override}

Profile.validator = ProfileValidator.init()
