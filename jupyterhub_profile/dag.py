class Node:

    def __init__(self, id):
        self.id = id
        self.dependencies = set()

    def add_dependency(self, dep):
        self.dependencies.add(dep)

    def __hash__(self):
        return hash(self.id)

class Graph(set):

    nop = lambda *args: None

    def walk(self, nodes, *, pre=nop, post=nop):
        if nodes is None:
            nodes = self

        for node in nodes:
            pre(node)

            self.walk(node.dependencies, pre=pre, post=post)
            
            post(node)

    def walk_once(self, nodes, *, pre=nop, post=nop):
        pre_visited = set()
        post_visited = set()
        
        def _pre(node):
            if node not in pre_visited:
                pre(node)
                pre_visited.add(node)

        def _post(node):
            if node not in post_visited:
                post(node)
                post_visited.add(node)

        self.walk(nodes, pre=_pre, post=_post)

    def enable_dependencies(self):
        def _enable(node):
            node.enabled = True

        enabled_nodes = { n for n in self if n.enabled }

        self.walk_once(enabled_nodes, pre=_enable)

    def topo_level_sort(self):
        """ Topological Sort of Profile dependencies
            Based on Kahn's Algirthm
        
        """

        # NodeDeps = recordclass.recordclass('NodeDeps', 'job dependencies', hashable=True)

        class NodeDeps:

            def __init__(self, node):
                self.node = node
                self.dependencies = node.dependencies.copy()

            def __hash__(self):
                return hash(self.node)

        levels = [ ]
        nodes = { NodeDeps(node) for node in self }

        while len(nodes) > 0:
            zero_degree = { n for n in nodes if len(n.dependencies) == 0 }
            nodes -= zero_degree

            level = { n.node for n in zero_degree }
            levels.append(level)

            for n in nodes:
                n.dependencies -= level

        return levels
