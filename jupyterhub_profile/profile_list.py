import pygraphviz as pgv
import glob
import os
import yaml
import copy
import jmespath

from jupyterhub_profile.dag import Graph

from jupyterhub_profile.profile import Profile
from jupyterhub_profile.dict_merge import dict_merge
from jupyterhub_profile.config import *

class ProfileList(Graph):

    def __init__(self, directory, profiles=set(), lint=False, dependencies=True):
        super().__init__()

        t = {}

        if profiles is not set:
            profiles = set(profiles)
    
        slugs = set()
        
        files = glob.glob(f'{directory}/**/*.yaml', recursive=True)
        files.sort()

        for fn in files:
            with open(fn) as f:
                p = yaml.safe_load(f)
                slug = p.get('slug')

                if slug is None:
                    raise RuntimeError('Missing slug', p, fn)

                if (slug.lower() + '.yaml') != os.path.basename(fn):
                    raise RuntimeError('Invalid slug. The slug has to match the filename!', p, slug.lower() + '.yaml', os.path.basename(fn))
                if slug in slugs:
                    raise RuntimeError('Duplicate slugs', p, fn)

                q = dict_merge(copy.deepcopy(t), p)

                en = bool({ q['slug'], 'all' } & profiles)

                try:
                    r = Profile(q, en, fn)
                except RuntimeError as r:
                    if lint:
                        continue
                    else:
                        raise r

                slugs.add(r.slug)
                self.add(r)

        if profiles - slugs and profiles != {'all'}:
            raise RuntimeError('Unknown profiles', profiles - slugs)

        self.resolve_dependencies()

        if dependencies:
            self.enable_dependencies()

    @property
    def data(self):
        d = [ p.data for p in self ]

        d.sort(key=lambda a: -a['priority'])

        return d

    def query(self, query):

        class CustomFunctions(jmespath.functions.Functions):
            @jmespath.functions.signature({'types': ['string']}, {'types': ['object']})
            def _func_format(self, f, o):
                return f.format(**o)

        if query:
            return jmespath.search(query, self.data, options=jmespath.Options(custom_functions=CustomFunctions()))
        else:
            return self.data

    def resolve_dependencies(self):
        slug_map = dict()

        for p in self:
            slug_map[p.slug] = p

        for p in self:
            dep = p.image.get('base')
            if dep is None:
                continue

            if dep not in slug_map:
                raise RuntimeError('Unknown dependency', p, dep)

            p.image['base'] = slug_map[dep]

            p.add_dependency(slug_map[dep])

    def build_images(self, skip_clone):
        enabled = { p for p in self if p.enabled }

        for p in enabled:
             p.build(skip_clone)
            
    def get_pipeline(self, format='gitlab', include_disabled=False):
        levels = self.topo_level_sort()

        if format != 'gitlab':
            raise NotImplementedError()

        config = {
            'stages': []
        }
        
        num = 1
        for jobs in levels:
            if not include_disabled:
                jobs = [ j for j in jobs if j.enabled ]

            if len(jobs) == 0:
                continue

            stage = 'stage%d' % num

            config['stages'].append(stage)

            for job in jobs:
                c = job.gitlab_ci_job
                if c:
                    config[job.slug.lower()] = {
                        'stage' : stage,
                        **c,
                    }
            num = num + 1

        return config

    def graph(self, path, format='svg', layout='neato', link_template=PROJECT_URL + '{}.yaml'):
        G = pgv.AGraph(directed=True)

        for p in self:
            G.add_node(p.slug)
            n = G.get_node(p.slug)

            if p.enabled:
                n.attr['color'] = 'hotpink'

            n.attr['URL'] = str.format(link_template, p.slug)
            
            for d in p.dependencies:
                G.add_edge(p.slug, d.slug)

        G.draw(path, format, layout)
