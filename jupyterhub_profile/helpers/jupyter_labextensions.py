#!/bin/env python3

import sys
import json

from jupyterlab.commands import _AppHandler, AppOptions

ao = AppOptions()
ah = _AppHandler(ao)

exts = ah.info['app_extensions'] + ah.info['sys_extensions']

data = [ { 'name': name,  **dict(ah.info['extensions'][name]) } for name in exts ]

print(json.dumps(data))
