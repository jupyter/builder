import sys
import json
import importlib

from jupyter_core.paths import jupyter_config_path
from notebook.config_manager import BaseJSONConfigManager

class ExtensionValidationError(Exception): pass

def validate_server_extension(import_name):
    """Tries to import the extension module. 
    Raises a validation error if module is not found.
    """
    try:
        mod = importlib.import_module(import_name)
        func = getattr(mod, 'load_jupyter_server_extension')
        version = getattr(mod, '__version__', '')
        return mod, func, version
    # If the extension does not exist, raise an exception
    except ImportError:
        raise ExtensionValidationError('{} is not importable.'.format(import_name))
    # If the extension does not have a `load_jupyter_server_extension` function, raise exception.
    except AttributeError:
        raise ExtensionValidationError('Found module "{}" but cannot load it.'.format(import_name))

exts = []
config_dirs = jupyter_config_path()
for config_dir in config_dirs:
    cm = BaseJSONConfigManager(config_dir=config_dir)
    data = cm.get("jupyter_notebook_config")
    server_extensions = (
        data.setdefault("NotebookApp", {})
        .setdefault("nbserver_extensions", {})
    )
    for import_name, enabled in server_extensions.items():
        ext = {
            'import_name': import_name,
            'enabled': enabled
        }

        try:
            mod, func, version = validate_server_extension(import_name)
            ext['name'] = mod.__package__
            ext['valid'] = True

            if version:
                ext['version'] = version
        
        except ExtensionValidationError as err:
            ext['name'] = import_name
            ext['valid'] = False

        exts.append(ext)

print(json.dumps(exts))
