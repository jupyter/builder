import os

FACULTY_NAMES = {
    1   : 'Mathematics, Computer Science and Natural Sciences',
    2   : 'Architecture',
    3   : 'Civil Engineering',
    4   : 'Mechanical Engineering',
    5   : 'Georesources and Materials Engineering',
    6   : 'Electrical Engineering and Information Technology',
    7   : 'Arts and Humanities',
    8   : 'School of Business and Economics',
    10  : 'Medicine'
}

FACULTY_LINKS = {
    1   : 'https://www.rwth-aachen.de/go/id/fvp/lidx/1',
    2   : 'http://arch.rwth-aachen.de/',
    3   : 'https://www.rwth-aachen.de/go/id/goh/lidx/1',
    4   : 'https://www.rwth-aachen.de/go/id/gxo/lidx/1',
    5   : 'https://www.rwth-aachen.de/go/id/hgv/lidx/1',
    6   : 'https://www.rwth-aachen.de/go/id/mp/lidx/1',
    7   : 'https://www.rwth-aachen.de/go/id/hqc/lidx/1',
    8   : 'https://www.rwth-aachen.de/go/id/hzj/lidx/1d',
    10  : 'http://www.medizin.rwth-aachen.de/cms/Fakultaeten/~iiq/Medizin/?lidx=1'
}

JUPYTERHUB_URL = 'https://jupyter.rwth-aachen.de'

PROJECT_URL = os.environ.get('CI_PROJECT_URL', 'https://git.rwth-aachen.de/jupyter/profiles')

DOCKER_REGISTRY = os.environ.get('REGISTRY_IMAGE', os.environ.get('CI_REGISTRY_IMAGE', 'registry.git.rwth-aachen.de/jupyter/profiles'))
DOCKER_BUILDER_IMAGE = os.environ.get('DOCKER_IMAGE', DOCKER_REGISTRY)
DOCKER_ACCOUNTS = [ ]

if os.environ.get('CI'):
    DOCKER_ACCOUNTS.append({
        'username' : os.environ.get('CI_REGISTRY_USER'),
        'password' : os.environ.get('CI_REGISTRY_PASSWORD'),
        'registry' : os.environ.get('CI_REGISTRY')
    })

if os.environ.get('REGISTRY'):
    DOCKER_ACCOUNTS.append({
        'username' : os.environ.get('REGISTRY_USER'),
        'password' : os.environ.get('REGISTRY_PASSWORD'),
        'registry' : os.environ.get('REGISTRY')
    })

JSON_FORMAT = {
    'indent': 4,
    'sort_keys': True
}
