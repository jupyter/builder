import cerberus
import glob
import os
import yaml
import spdx_license_list
import re
import pkg_resources
import os

from jupyterhub_profile.util import uri_validator
from jupyterhub_profile.config import *

class ProfileValidator(cerberus.Validator):

    def _check_with_spdx_license(self, field, value):
        if value is not None and value not in spdx_license_list.LICENSES:
            self._error(field, "Must be an a valid SPDX license identifier: https://spdx.org/licenses/")

    def _check_with_url(self, field, value):
        if not uri_validator(value):
            self._error(field, "Must be an a valid URL")

    def _normalize_coerce_email(self, value):
        r = re.compile(r"^([\s\w.-]+)\s+<([\w\.@-]+)>$")
        m = re.match(r, value)

        return {
            'name': m.group(1),
            'mail': m.group(2)
        }

    def _normalize_coerce_faculty(self, value):
        if value not in FACULTY_NAMES:
            raise RuntimeError('Invalid faculty')

        return {
            'number': value,
            'name': FACULTY_NAMES[value],
            'url': FACULTY_LINKS[value]
        }

    @classmethod
    def init(cls):
        v =  ProfileValidator(error_handler=cerberus.errors.BasicErrorHandler(), purge_unknown=True)

        for fn in pkg_resources.resource_listdir(__name__, 'schema'):
            st = pkg_resources.resource_stream(__name__, os.path.join('schema', fn))
            y = yaml.safe_load(st)
            if '_values.yaml' in fn:
                for k in y:
                    v.rules_set_registry.add(k, y[k])
            else:
                g = os.path.basename(fn).replace('.yaml', '')

                v.schema_registry.add(g, y)

        v.schema = v.schema_registry.get('profile')

        return v
