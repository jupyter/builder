# Jupyter Profile Builder


### Usage

```
usage: jupyterhub-profile [-h] [--dir DIR] [--profiles PROFILES] [--verbose] [--no-dependencies] CMD ...

positional arguments:
  CMD                   sub-command help
    graph               Visuzalize profile dependencies via Graphviz
    build               Build Docker images for selected profiles
    list                Generate Jupyter Profile configuration
    pipeline            Generate CI job configuration
    lint                Validate profile definitions
    query               Query the profile descriptions
    versions            Extract Python packages and version from Docker image

optional arguments:
  -h, --help            show this help message and exit
  --dir DIR, -d DIR     Directory with profile files
  --profiles PROFILES, -p PROFILES
                        Select profiles. All if omitted
  --verbose, -v
  --no-dependencies, -n
                        Should we build dependencies before?
```

#### Visuzalize profile dependencies via Graphviz

```
usage: jupyterhub-profile graph [-h]
                             [--format {canon,cmap,cmapx,cmapx_np,dia,dot,fig,gd,gd2,gif,hpgl,imap,imap_np,ismap,jpe,jpeg,jpg,mif,mp,pcl,pdf,pic,plain,plain-ext,png,ps,ps2,svg,svgz,vml,vmlz,vrml,vtx,wbmp,xdot,xlib}]
                             [--layout {neato,dot,twopi,circo,fdp,nop}]
                             FILE

positional arguments:
  FILE

optional arguments:
  -h, --help            show this help message and exit
  --format {canon,cmap,cmapx,cmapx_np,dia,dot,fig,gd,gd2,gif,hpgl,imap,imap_np,ismap,jpe,jpeg,jpg,mif,mp,pcl,pdf,pic,plain,plain-ext,png,ps,ps2,svg,svgz,vml,vmlz,vrml,vtx,wbmp,xdot,xlib}, -f {canon,cmap,cmapx,cmapx_np,dia,dot,fig,gd,gd2,gif,hpgl,imap,imap_np,ismap,jpe,jpeg,jpg,mif,mp,pcl,pdf,pic,plain,plain-ext,png,ps,ps2,svg,svgz,vml,vmlz,vrml,vtx,wbmp,xdot,xlib}
  --layout {neato,dot,twopi,circo,fdp,nop}, -l {neato,dot,twopi,circo,fdp,nop}
```

#### `build`: Build Docker images for selected profiles

```
usage: jupyterhub-profile build [-h] [--skip-clone]

optional arguments:
  -h, --help        show this help message and exit
  --skip-clone, -s  Skip clone of Git repo if it already exists.
```

#### `list`: Generate Jupyter Profile configuration

```
usage: jupyterhub-profile list [-h] [--format {jupyter,yaml,json,simple}]

optional arguments:
  -h, --help            show this help message and exit
  --format {jupyter,yaml,json,simple}, -f {jupyter,yaml,json,simple}
```

#### `pipeline`: Generate CI job configuration

```
usage: jupyterhub-profile pipeline [-h] [--format {gitlab}]

optional arguments:
  -h, --help            show this help message and exit
  --format {gitlab}, -f {gitlab}
```

#### `lint`: Validate profile definitions

```
usage: jupyterhub-profile lint [-h]

optional arguments:
  -h, --help  show this help message and exit
```

#### `query`: Query the profile descriptions

```
usage: jupyterhub-profile query [-h] [--query QUERY] [--format {json,yaml,raw}]

optional arguments:
  -h, --help            show this help message and exit
  --query QUERY, -q QUERY
                        A JMESpath query
  --format {json,yaml,raw}, -f {json,yaml,raw}
                        Output format
```

#### `versions`: Extract Python packages and version from Docker image

```
usage: jupyterhub-profile versions [-h] --repo {conda,pip}

optional arguments:
  -h, --help            show this help message and exit
  --repo {conda,pip}, -r {conda,pip}
```

### Query Examples


#### Get all profile contacts

```bash
jupyterhub-profile query -q "[].contacts[][format('{name} <{mail}>', @)][]" -f raw
```


#### Get profiles from faculty 6

```bash
jupyterhub-profile query -q '[?course.faculty.number==`6`][display_name][]' -f raw
```
