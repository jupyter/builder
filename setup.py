from setuptools import setup, find_packages

setup(
    name='jupyterhub-profile',
    description='Build singleuser Docker images for JupyterHub',
    version='0.1',
    packages=find_packages(),
    author='Steffen Vogel',
    author_email='post@steffenvogel.de',
    keywords='docker jupyterhub jupyter',
    url='https://git-ce.rwth-aachen.de/jupyter/profiles/builder',
    entry_points={
        'console_scripts': [
            'jupyterhub-profile = jupyterhub_profile.builder:main'
        ],
    },
    install_requires=[
        'PyYAML',
        'podman',
        'gitpython',
        'recordclass',
        'termcolor',
        'pygraphviz',
        'cerberus',
        'spdx-license-list',
        'furl',
        'jmespath'
    ],
    python_requires='>=3.5',
    include_package_data=True,
    zip_safe=False
)
